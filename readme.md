# CamZMQ

This script will stream the raspberry pi camera feed over a zeromq tcp socket.


## Usage

Run with:

    python camzmq.py

The following will be broadcast:

* Port 5000: the raw camera image
* Port 5001: a thresholded image

There is sample code for how to listen to a broadcast stream in `sample-client.py`.

You can change the camera settings (shutter speed, etc.) by modifying `settings.json`. It will update in real time.


## Installation on Raspberry Pi

Here are all the things you need to install (on Raspbian Lite) to make this work:

    sudo apt-get update
    sudo apt-get upgrade

    sudo apt-get install build-essential python-dev python-pip

    sudo apt-get install python-opencv

    sudo pip install "picamera[array]"
    sudo pip install imutils

    sudo apt-get install libzmq3-dev
    sudo pip install pyzmq

    sudo pip install commentjson

Ensure the camera is enabled here:

    sudo raspi-config

You can test the camera with:

    raspistill -t 0
